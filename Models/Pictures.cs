﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpreadJsDemoApi
{
    public class Pictures:BaseParent
    {
        public int Id { get; set; }
        /// <summary>
        /// 表头ID
        /// </summary>
        public int TableHeadId { get; set; }
        /// <summary>
        /// 图片名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图片路径Base64
        /// </summary>
        public string Src { get; set; }
        /// <summary>
        /// 横坐标
        /// </summary>
        public int PostionX { get; set; }
        /// <summary>
        /// 纵坐标
        /// </summary>
        public int PostionY { get; set; }
        /// <summary>
        /// 宽
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// 高
        /// </summary>
        public int Height { get; set; }
    }
}
